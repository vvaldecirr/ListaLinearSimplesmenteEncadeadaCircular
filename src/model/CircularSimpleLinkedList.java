package model;

public class CircularSimpleLinkedList {
	private Node	sna; // some node address
	private boolean	ordered; // kind of List with true for ordered and false for unordered

	public CircularSimpleLinkedList(boolean ordered) {
		// this.sna = null; // doesn't needed on Java
		this.setOrdered(ordered);
	}

	public boolean isOrdered() {
		return this.ordered;
	}

	public void setOrdered(boolean ordered) {
		this.ordered = ordered;
	}

	// answer true for empty list and false for not empty
	public boolean isEmpty() {
		if (this.sna == null)
			return true;
		else
			return false;
	}

	// add a Node to list
	public void addNode(Node node) {
		// if List is empty
		if (this.isEmpty())
			this.sna = node;
		else {
			if (this.isOrdered())
				this.addOrderedNode(node);
			else
				this.addUnorderedNode(node);
		}
	}
	
	// add a Node in unordered sequence to list always remembering the last one inserted
	public void addUnorderedNode(Node node) {
		// if just have one node on list, make the Nodes point to each other
		if (this.sna.getNextNode() == null) {
			node.setNextNode(this.sna);
			this.sna.setNextNode(node);
			
		} else { // if more than one on list, place the Node in front of remembered node (sna)
			node.setNextNode(this.sna.getNextNode());
			this.sna.setNextNode(node);
		}

		this.sna = node;
	}

	// add a Node in ordered sequence to list always remembering the last one of list
	public void addOrderedNode(Node node) {
		// if just have one node on list, make the Nodes point to each other
		if (this.sna.getNextNode() == null) {
			node.setNextNode(this.sna);
			this.sna.setNextNode(node);
			
			// remembering the righter value Node
			if (node.getValue() > this.sna.getValue())
				this.sna = node;
			
		} else { // if more than one on list, place the Node in correct position
			// if the value is smaller than the first Node, place at beginning
			if (node.getValue() < this.sna.getNextNode().getValue()) {
				node.setNextNode(this.sna.getNextNode());
				this.sna.setNextNode(node);
				
			} else if (node.getValue() > this.sna.getValue()) {// if the value is bigger than the last Node, place at end
				node.setNextNode(this.sna.getNextNode());
				this.sna.setNextNode(node);
				this.sna = node;
				
			} else { // else search the right position for place the Node
				Node aux = this.sna.getNextNode();
				
				// until reach the end of list search for right position
				while (aux.getValue() != this.sna.getValue()) {
					if (aux.getNextNode().getValue() > node.getValue()) {
						node.setNextNode(aux.getNextNode());
						aux.setNextNode(node);
						
						aux = this.sna;
					}
					
					if (aux != this.sna)
						aux = aux.getNextNode();
				}
			}
		}
	}

	// remove a Node found on searched value from List
	public boolean removeNode(int value) {
		// if the list is empty, impossible to remove
		if (this.sna == null)
			return false;
		else {
			// if the the Node for to be excluded is the first one, make the last point to the second one
			if (value == this.sna.getNextNode().getValue()) {
				this.sna.setNextNode(this.sna.getNextNode().getNextNode());
				
				return true;
				
			} else {// search on list for the Node before that choose to be excluded 
				Node aux = this.findByValue(value);
				
				if (aux != null) {
					// checking if the last element is the highest one
					if (aux.getNextNode().getValue() == this.sna.getValue()) 
						this.sna = aux;
					
					// make the Node returned point to the second Node after
					aux.setNextNode(aux.getNextNode().getNextNode());
					
					return true;
					
				} else 
					return false;
			}			
		}
	}
	
	// search a Node according to the given value returning the Node before of that found
	public Node findByValue(int value) {
		Node aux = this.sna.getNextNode();
		
		// while didn't found the Node or reach the end of the list, search
		while (aux.getValue() != this.sna.getValue()) {
			if (aux.getNextNode().getValue() == value)
				return aux;
			else
				aux = aux.getNextNode();
		}
		
		return null;
	}

	// show the actual status of List's arrangements data
	public String showList() {
		String kind = "não ordenada";
		String text = "";

		if (this.ordered)
			kind = "ordenada";
		
		// if the list isn't empty
		if (!this.isEmpty()) {
			// if just have one element at list
			if (this.sna.getNextNode() == null) 
				text = String.valueOf(this.sna.getValue());
			else {
				Node aux = this.sna.getNextNode();
				
				do {
					text += aux.getValue() + " | ";
					aux = aux.getNextNode();
				} while (aux.getValue() != this.sna.getNextNode().getValue());			
			}
		} else
			text = "lista vazia";
		
		return "Lista tipo "+kind+"\n"+text;
	}
}