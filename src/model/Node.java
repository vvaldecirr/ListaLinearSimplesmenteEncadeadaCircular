package model;

public class Node {
	private int value;
	private Node nextNode;

	public Node(int value) {
		this.setValue(value);
		// this.setNextNode(null); // doesn't needed on Java
	}

	public int getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getNextNode() {
		return this.nextNode;
	}

	public void setNextNode(Node nextNode) {
		this.nextNode = nextNode;
	}
}
