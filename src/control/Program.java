package control;

import model.CircularSimpleLinkedList;
import model.Node;

public class Program {

	public static void main(String[] args) {
		CircularSimpleLinkedList l1 = new CircularSimpleLinkedList(false);
		
		System.out.println(l1.showList());

		Node n1 = new Node(5);
		Node n2 = new Node(8);
		Node n3 = new Node(6);
		Node n4 = new Node(2);
		Node n5 = new Node(4);
		Node n6 = new Node(10);
		Node n7 = new Node(1);
		Node n8 = new Node(7);
		Node n9 = new Node(3);
		Node n10 = new Node(9);

		l1.addNode(n1);
		l1.addNode(n2);
		l1.addNode(n3);
		l1.addNode(n4);
		l1.addNode(n5);
		
		System.out.println(l1.showList());
		
		l1.removeNode(5);
		l1.removeNode(8);
		l1.removeNode(2);
		
		System.out.println(l1.showList());

		l1.addNode(n9);
		l1.addNode(n10);
		l1.addNode(n6);
		l1.addNode(n7);
		l1.addNode(n8);

		System.out.println(l1.showList());
	}

}
